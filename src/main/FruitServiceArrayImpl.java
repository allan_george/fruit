package main;

import java.io.FileWriter;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;


public  class FruitServiceArrayImpl implements FruitService
{
	private Fruit[] fruits;
	int j=0;
	 
	public FruitServiceArrayImpl(int size) {
		fruits=new Fruit[size];
	}
	
	public void add(Fruit fruit)
	{
		boolean flag=true;
		if(j>=fruits.length)
			System.out.println("Limit Exceeded");
		else if(flag==true)
		{
			
			
			for (int i=0;i<j;i++) 
			{
				if (fruits[i].equals(fruit)) {
					System.out.println("Duplicate");
					flag = false;
		}
		
	}
		}
			if(flag==true)
			{
				fruits[j]=fruit;
				j++;
			}
		}
	
	
	public void display()
	{
		for (int i = 0; i < j; i++) {
			System.out.println(fruits[i]);
		}
	}
	
	public void search(String name)
	{
		boolean flag = false;
		for (int i=0; i<j;i++) 
		{
			if (fruits[i].getName().equalsIgnoreCase(name)) {
				System.out.println(fruits[i]);
				flag = true;
			}
		}
		if (flag == false)
			System.out.println("No data To display");
		
	}
	
	public void sort()
	{
		Arrays.parallelSort(fruits,0,j);
		display();
		
	}

	public List<Fruit> search(LocalDate manufacturedDate) {

		 boolean flag = false;
			for (int i = 0; i < j; i++) {
				if (fruits[i].getManufacturedDate().isAfter(manufacturedDate)) {
					System.out.println(fruits[i]);
					flag = true;
				}
		
	}
	
			return null;
			}
	
	public void download() throws Exception {

		FileWriter file = new FileWriter("FruitCSV.csv");
		for (int i= 0;i<j;i++) {
			String data = csvConversion(fruits[i]);
			file.write(data);

		}
		file.close();
	}
	
	public void downloadJson() throws Exception{
		String data;
		
			FileWriter file;
			file = new FileWriter("FruitJson.json");
			for (int i = 0; i < j; i++) {
				if (i == 0)
					data = "[" + asJson(fruits[i]);
				else
					data = "," + asJson(fruits[i]);
				if (i == j - 1)
					data = data + "]";
				file.write(data);
			}
			file.close();
		
	}

	@Override
	public void delete(String name)
	{
		int k = 0;
		for (int i = 0; i < j; i++) {
		if (!fruits[i].getName().equals(name)) {
			fruits[k] = fruits[i];
		k++;
		System.out.println("deleted\n");
		}
		}
		j = k;
		}

	
}


