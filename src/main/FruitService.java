package main;


import java.time.LocalDate;
import java.util.List;

public interface FruitService {
	
	void add(Fruit fruit);
	void display();
	void search(String name) throws Exception;
	void sort();
	void delete(String name);
	
	
	
	List<Fruit> search(LocalDate manufacturedDate) throws Exception;
	
	void download() throws Exception;
	default String csvConversion(Fruit fruit)
	{
		return new StringBuffer("Name :").append(fruit.getName()).append("\nWeight :").append(fruit.getWeight())
				.append("\ncolor :").append(fruit.getColor()).append("\nDate Of Manufacture:").append(fruit.getManufacturedDate())
				.append("\nCategory:").append(fruit.getCategory()).append("\n").toString();
		
	}
	
	
	
	void downloadJson() throws Exception;
	 default String asJson(Fruit fruit) {
		 return new StringBuffer().append("{").append("\"Name\":").append("\"").append(fruit.getName()).append("\"")
				 .append(",\"Weight\":").append("\"").append(fruit.getWeight()).append("\"").append(",")
				 .append("\"Color\":").append(fruit.getColor()).append(",").append("\"Date of Manufacture\":")
				 .append("\"").append(fruit.getManufacturedDate()).append("\"").append(",").append("\"Category\":")
				 .append("\"").append(fruit.getCategory()).append("\"").append("}").append("\n").toString();
	       }
	
	
}
