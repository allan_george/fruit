package main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public  class FruitServiceArrayListImpl implements FruitService
{
	int j=0;
	List<Fruit> fruitList=new ArrayList<>();
	@Override
	public void add(Fruit fruit)
	{
		fruitList.add(new Fruit());
		
	}

	@Override
	public void display() 
	{
		fruitList.forEach(data->System.out.println(data));
		
	}

	@Override
	public void search(String name) {
		for(Fruit search : fruitList)
		{
			if(search.getName().equalsIgnoreCase(name))
			{
				System.out.println(name);
			}
		}
		
	}

	@Override
	public void sort() {
		Collections.sort(fruitList);
		display();
		
	}

	public List<Fruit> search(LocalDate manufacturedDate) {
		fruitList.stream()
		.filter(fruitList->fruitList.getManufacturedDate().isAfter(manufacturedDate))
		.collect(Collectors.toList());
		return fruitList;
	}

	@Override
	public void download() throws Exception {
		
		FileWriter file = new FileWriter("download.csv");
		for (int i = 0; i < j; i++) {
			String data = csvConversion(fruitList.get(i));
			file.write(data);
		}
		file.close();
	}
	@Override
	public void downloadJson() throws IOException {
		String data;
		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter file = null;
		try {
			file = new FileWriter("download.json");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Fruit fruit : fruitList) {
		data = asJson(fruit);
		join.add(data);
		}file.write(join.toString());
		file.close();
	}

	@Override
	public void delete(String name) {
		
		for(Iterator<Fruit> iterator=fruitList.iterator();iterator.hasNext();)
		{
			Fruit fruit=iterator.next();
			if(fruit.getName().equals(name)) {
				iterator.remove();
			}
		}
		}
		
	}