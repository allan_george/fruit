package main;

import java.time.LocalDate;

import java.util.Objects;



public  class Fruit implements Comparable<Fruit> 
{
	private String name;
	private int weight;
	private String color;
	private LocalDate manufacturedDate;
	private Category category;
	
	
	
	        
	      public Fruit(String name,int weight,String color,LocalDate date,Category category)
	        {
	        	
	            
	        	this.name=name;
	        	
	        	this.weight=weight;
	        	
	        	this.color=color;
	        	
	        	this.manufacturedDate=date;
	        	
	        	this.category=category;
	            
	        	
	        	
	        	}
	        	        
	        public Fruit() {
	        
	        }
			public String getName() {
			return name;
		}


		public void setName(String name) {
			this.name = name;
		}


		public int getWeight() {
			return weight;
		}


		public void setWeight(int weight) {
			this.weight = weight;
		}


		public String getColor() {
			return color;
		}


		public void setColor(String color) {
			this.color = color;
		}


		public LocalDate getManufacturedDate() {
			return manufacturedDate;
		}


		public void setManufacturedDate(LocalDate manufacturedDate) {
			this.manufacturedDate = manufacturedDate;
		}


		public Category getCategory() {
			return category;
		}


		public void setCategory(Category category) {
			this.category = category;
		}


			public String toString()
	        {
				return new StringBuffer("Name :").append(name).append("\nWeight :").append(weight)
						.append("\ncolor :").append(color).append("\nDate Of Manufacture:").append(manufacturedDate)
						.append("\nCategory:").append(category).append("\n").toString();
	        }
			
			
			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				Fruit other = (Fruit) obj;
				return weight == other.weight && Objects.equals(manufacturedDate, other.manufacturedDate) && category == other.category
						&& Objects.equals(name, other.name);
			}
			
			public int compareTo(Fruit fruits)
			{
				return weight-fruits.weight;
				
			}
		

			
}
			

	
			





