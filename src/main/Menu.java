package main;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Scanner;

public class Menu {

	public static void main(String[] args) throws Exception {
		
		FileReader reader=new FileReader("src/config.properties");
		Properties properties=new Properties();
		properties.load(reader);
		
		
		
	Scanner scn=new Scanner(System.in);
	
	//ArrayList<FruitServiceArrayImpl> a=new ArrayList<FruitServiceArrayImpl>();
	FruitService fruitService=FruitServiceFactory.getFruitService("Database",properties);
	
	int ch;
	do
	{
	System.out.println("Enter your choice \n1:Add\n2:Display\n3:Search\n4:Sort\n5:Search By Manufactured Date\n6:Delete\n7:Download as CSV\n8:Download As Json\n9:Exit");
	 ch=scn.nextInt();
	
	switch(ch)
	{
	case 1:
	{
		try{ 
			System.out.println("Enter the name of the fruit");
			String name=scn.next();
		    System.out.println("Enter the weight of the fruit:");
		    int weight=scn.nextInt();
		    System.out.println("Enter the color of the fruit:");
		    String color=scn.next();
		    System.out.println("Enter Manufactured Date");
		    String date=scn.next();
		    LocalDate dateOfManufacture=LocalDate.parse(date);
		    System.out.println("Enter Category");
		    String categoryvalue=scn.next();
		    Category category=Category.valueOf(categoryvalue.toUpperCase());
		    Fruit fruit =new Fruit(name,weight,color,dateOfManufacture,category);
		    fruitService.add(fruit);
		    }
		    catch(InputMismatchException e){
			System.out.println(e.getMessage());
			scn.nextLine();}
	    break;
	
	}
	case 2:
		
		fruitService.display();
		
	break;
		
	case 3:
	
		System.out.println("Enter name of the fruit to be searched:");
		String name=scn.next();
		fruitService.search(name);
	 
		break;		
	case 4:
	
		fruitService.sort();
		
	
		break;
		
		
	case 5:		
	
		String date=scn.next();
		LocalDate dateOfManufacture=LocalDate.parse(date);
		fruitService.search(dateOfManufacture);
	
	break;
		
	case 6:
		
	{   System.out.println("Enter Name of Fruit To be Deleted\n");
    String name1=scn.next();
    fruitService.delete(name1);
    break;
}
	
	case 7:	
		
	{   try {
		fruitService.download();
		
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
		break;
		
	}
			
	case 8:
		
	{   try {
		fruitService.downloadJson();
		}
	    catch(Exception e) 
	{
		e.printStackTrace();
	}
		break;
	} 
			
	case 9:
				System.exit(0);
				

		
		default:System.out.println("wrong choice:");
	}}
	
	while(ch!=9);


	scn.close();
	
}
}