package main;

import java.util.Properties;

public abstract class FruitServiceFactory {

	public static FruitService getFruitService(String type,Properties properties)
	{
		
		FruitService fruitService=null;
		switch(type) 
		{
		case "Array":
			fruitService=new FruitServiceArrayImpl(10);
			break;
		case "ArrayList":
			fruitService=new FruitServiceArrayListImpl();
			break;
		case "Database":
			
			String user=properties.getProperty("user");
			String url=properties.getProperty("url");
			String driver=properties.getProperty("driver");
			String password=properties.getProperty("password");
			String implementation=properties.getProperty("implementation");
			fruitService=new FruitServiceDatabaseImplementation(user,url,driver,password);
			break;
		}
	   return fruitService;
	}

}
