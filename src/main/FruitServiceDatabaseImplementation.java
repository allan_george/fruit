package main;

import java.io.FileWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class FruitServiceDatabaseImplementation implements FruitService {
	String user,password,driver,url;

	public FruitServiceDatabaseImplementation(String url,String user,String password,String driver)
	{
		this.user=user;
		this.password=password;
		this.driver=driver;
		this.url=url;
		
	}

	private Connection getConnection() throws ClassNotFoundException,SQLException
	
	{

		Class.forName(driver);
		return DriverManager
				.getConnection(url,user,password);
	} 
		
	
		
	
	
	
	public void add(Fruit fruit) {
		
		try {
			Connection connection=getConnection();
			String str="INSERT INTO Fruit(name,weight,color,manufactured_Date,category) VALUES(?,?,?,?,?)";
			
			PreparedStatement statement=connection.prepareStatement(str);
			statement.setString(1,fruit.getName());
			statement.setInt(2,fruit.getWeight());
			statement.setString(3,fruit.getColor());
			statement.setDate(4,Date.valueOf(fruit.getManufacturedDate()));
			statement.setString(5,fruit.toString());
			
			statement.executeUpdate();
			connection.close();
		
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	public void display() {
		
		try {
			Connection connection=getConnection();
			String str="SELECT * FROM FRUIT";
			PreparedStatement statement=connection.prepareStatement(str);
			ResultSet result=statement.executeQuery();
			while(result.next())
			{  
				Fruit fruit=getFruit(result);
				System.out.println(fruit);

			}
			
			connection.close();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	public void search(String name) throws Exception {
		
		try {
			Connection connection=getConnection();
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("SELECT * FROM fruit  where name=?");
			prestatement.setString(1,name);
			ResultSet resultSet=prestatement.executeQuery();
			while(resultSet.next())
			{
				Fruit fruit=getFruit(resultSet);
				System.out.println(fruit);
			}
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
		  e.printStackTrace();
		}
		
		
		
	}

	
	public void sort() {
		try {
			Connection connection=getConnection();
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("SELECT * FROM fruit order by name");
			ResultSet resultSet=prestatement.executeQuery();
			while(resultSet.next())
			{
				Fruit fruit=getFruit(resultSet);
				System.out.println(fruit);
			}
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
		  e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	public void delete(String name) {
		try {
			Connection connection=getConnection();
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("DELETE FROM fruit  where name=?");
			prestatement.setString(1,name);
			prestatement.executeUpdate();
			System.out.println("Data Deleted From DataBase");
			connection.close();
			
		} catch (ClassNotFoundException | SQLException e) {
		  e.printStackTrace();
		}

	}
	
	
	
	 private Fruit getFruit(ResultSet resultSet) throws Exception 
	 { 
		    Fruit fruit = new Fruit();
		    fruit.setName(resultSet.getString("Name"));
		    fruit.setWeight(resultSet.getInt("Weight"));
		    fruit.setColor(resultSet.getString("Color"));
		    fruit.setManufacturedDate(resultSet.getDate("Date of Manufacture").toLocalDate());
		    fruit.setCategory(Category.valueOf(resultSet.getString("Category")));
			return fruit;
		     }
	


	@Override
	public List<Fruit> search(LocalDate manufacturedDate) throws Exception  {
		
		List<Fruit> FruitList=new ArrayList<>();
		try {
		Connection connection = getConnection();
		connection = getConnection();
		PreparedStatement preparedStatement = connection
				.prepareStatement("SELECT * FROM fruit where manufactured_date>?;");
		preparedStatement.setDate(1,Date.valueOf(manufacturedDate));
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
		FruitList.add(getFruit(resultSet));
		}
		connection.close(); } 
		catch (ClassNotFoundException | SQLException e) {
		e.printStackTrace();
		}
		return FruitList;
		}
	

	
	public void download() throws Exception {
		 Connection connection;
		    try {
			connection = getConnection();
			PreparedStatement prestatement = connection.prepareStatement("select * from fruit");
			ResultSet resultSet = prestatement.executeQuery();
			FileWriter file = new FileWriter("Fruit.csv");
			while (resultSet.next()) {
			String data = csvConversion(getFruit(resultSet));
			file.write(data);
			}
			file.close();
			connection.close();
			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}

	}

	
	public void downloadJson() throws Exception {
		Connection connection;
		try {
			String data;
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("select * from fruit");
		ResultSet resultSet = prestatement.executeQuery();
		FileWriter file = new FileWriter("Fruit.json");
		StringJoiner join = new StringJoiner(",", "[", "]");
		while (resultSet.next()) {
		data = asJson(getFruit(resultSet));
		join.add(data);
		}file.write(join.toString());
		file.close();
		connection.close();
		} catch (ClassNotFoundException | SQLException e) {
		e.printStackTrace();
		}

	

	}

}
